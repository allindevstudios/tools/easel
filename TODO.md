### TODO

To have a first very basic Pixelart editor, these need to be added:

* V - Undo (ctrl + Z)
* V - Visual layer selection
* Better representation of canvas (allow for translate + zoom canvas too...)
* V - Refactor

* Make this file better and more complete, write docs...

Additionally I want to do this:

* V - Draw between points when moving, not just on the points
* Have a rectangle tool
* V Have a fill tool
* Have a line tool
* Show the current pixel position (and tool information) along with zoom at the bottom of the canvas
* Dynamic UI scaling when resizing + scrollbars in widgets

Things currently out of scope are

* Select tool
* Original isometric tileset drawing functionality
* Hotkeys for different tool combinations
* Have a dither brush



