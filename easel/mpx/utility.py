from typing import Dict, Mapping, List

import os

from PySide2.QtGui import QImage


def make_mpx_filename(filename: str):
    path = os.path.dirname(filename)
    filename = os.path.basename(filename)
    if not filename.endswith(".mpx"):
        filename = '.'.join(filename.split(".")[:-1]) + ".mpx"
    return os.path.join(path, filename)


def make_colors_from_layer(layer: QImage) -> Dict[str, int]:
    colors = {}
    for x in range(layer.width()):
        for y in range(layer.height()):
            color = hex(layer.pixel(x, y))
            if color not in colors:
                colors[color] = len(colors)
    return colors


def make_contents_from_layer(layer: QImage, colors):
    contents = []
    for x in range(layer.width()):
        for y in range(layer.height()):
            color = hex(layer.pixel(x, y))
            contents.append(str(colors[color]))
    return contents


def get_color_array(colors_dict: Mapping) -> List:
    color_array = [''] * len(colors_dict)
    for color, number in colors_dict.items():
        color_array[number] = color
    return color_array


def make_layer_from_contents(contents, colors, width, height):
    layer = QImage(width, height, QImage.Format_ARGB32)
    pixel_index = 0
    for x in range(width):
        for y in range(height):
            layer.setPixel(x, y, colors[contents[pixel_index]])
            pixel_index += 1
    return layer
