from typing import Sequence, Mapping

from PySide2.QtGui import QImage, qRgba

from easel.mpx.utility import (make_mpx_filename, make_layer_from_contents, make_colors_from_layer, make_contents_from_layer,
                               get_color_array)


# TODO: currently has a lot of shortcomings for bigger files... address boundary errors (2.5kx1.4k ~ 4 Mio indices
class MPX:
    def __init__(self, filename: str, layers: Sequence[QImage], meta: Mapping = None):
        self._filename = make_mpx_filename(filename)
        self._layers = layers
        self._colors = {}
        self._contents = []
        self._meta = meta

        self.make_colors()
        self.make_contents()

    def save(self):
        try:
            with open(self._filename, 'w') as mpx_file:
                return mpx_file.write(self.save_data) > 0
        except:
            return False

    def load(self):
        layers = []
        with open(self._filename, 'r') as mpx_file:
            lines = mpx_file.readlines()
            dimension_line = lines[1].strip()
            color_line = lines[2].strip()
            contents_lines = [line.strip() for line in lines[3:]]
            width, height = [int(dimension) for dimension in dimension_line.split(',')]
            colors = [qRgba64(int(color, 16)) for color in color_line.split(',')]
            layers = [make_layer_from_contents([int(color_number) for color_number in contents_line.split(',')], colors,
                                               width, height) for contents_line in contents_lines]
        return layers

    def make_colors(self):
        layers_colors = [make_colors_from_layer(layer) for layer in self._layers]
        unique_colors = {}
        for layer_colors in layers_colors:
            for color in layer_colors:
                if color not in unique_colors:
                    unique_colors[color] = len(unique_colors)
        self._colors = unique_colors

    def make_contents(self):
        self._contents = [make_contents_from_layer(layer, self.colors) for layer in self._layers]

    @property
    def save_data(self):
        s = 'META:'
        # meta info
        s += '\n{},{}'.format(self.layers[0].width(), self.layers[0].height())
        s += '\n' + ','.join(get_color_array(self._colors))
        for layer_contents in self._contents:
            s += '\n' + ','.join(layer_contents)
        return s

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, filename):
        self._filename = make_mpx_filename(filename)

    @property
    def layers(self):
        return self._layers

    @layers.setter
    def layers(self, layers):
        self._layers = layers
        self.make_colors()
        self.make_contents()

    @property
    def meta(self):
        return self._meta

    @meta.setter
    def meta(self, meta):
        self._meta = meta

    @property
    def colors(self):
        return self._colors

    @property
    def contents(self):
        return self._contents