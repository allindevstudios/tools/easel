from PySide2.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout, QPushButton, QTabWidget, QFileDialog, QAction, QMainWindow, qApp
from PySide2.QtGui import QIcon, QImage, QPainter, QColor
from PySide2.QtCore import QStandardPaths, Qt, QTimer

from easel.ui.toolbox.palettes.palette_manager import PaletteManager
from easel.ui.canvas.canvas_graphicsview import Canvas
from easel.ui.toolbox.layers import LayerBar
from easel.ui.toolbox.preview.preview import Preview
from easel.ui.dialogs.new_file_dialog.new_file_dialog import NewFileDialog
from easel.mpx.mpx import MPX
from easel.ui.toolbox.toolbar import Tools
from easel.data_structures.layer import Layer
from easel.data_structures.layers import Layers
from easel.undo_stack.undo_stack import UndoStack


from easel.utility.files import Resources



class Easel(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

        self.show()

    def initUI(self):
        self.pixel_edit = PixelEdit(self)
        self.setCentralWidget(self.pixel_edit)

        self.setGeometry(400, 400, 900, 400)

        self.setWindowTitle("Easel")
        self.setWindowIcon(QIcon(Resources().main_icon))

        file_menu = self.menuBar().addMenu("&File")
        view_menu = self.menuBar().addMenu("&View")

        self.addActionToMenu("&New", "Ctrl+N", self.new, menu=file_menu)
        self.addActionToMenu("&Load", "Ctrl+O", self.load, menu=file_menu)
        self.addActionToMenu("&Save", "Ctrl+S", self.save, menu=file_menu)
        self.addActionToMenu("&Export", "Ctrl+E", self.export, menu=file_menu)
        self.addActionToMenu("&Replay", "", self.replay, menu=file_menu)
        # TODO: make new in the long run

        self.addActionToMenu("&Exit", "Ctrl+Q", qApp.quit, menu=file_menu)

        self.addActionToMenu("&Toggle Menu", "Ctrl+M", self.toggleMenu, menu=view_menu)

    def addActionToMenu(self, name, shortcut, on_triggered, menu=None):
        action = QAction(name, self)
        action.setShortcut(shortcut)
        action.triggered.connect(on_triggered)
        self.addAction(action)
        if menu is not None:
            menu.addAction(action)
        return action

    def toggleMenu(self):
        self.menuBar().hide() if self.menuBar().isVisible() else self.menuBar().show()

    def replayUpdate(self):
        if self.pixel_edit._undo_stack.redo():
            QTimer.singleShot(200, self.replayUpdate)

    def replay(self):
        self.pixel_edit._undo_stack.reset_to_start()
        # self.pixel_edit.layersUpdated()
        QTimer.singleShot(200, self.replayUpdate)
        # while self.pixel_edit._undo_stack.redo():
        #     print("at least once?")
        #     import time
        #     time.sleep(0.1)
        #     self.pixel_edit.layersUpdated()

    def new(self):
        new_file_dialog = NewFileDialog(self)
        contents = new_file_dialog.getNewFileStats()
        # new_file_dialog.show()
        if contents is not None:
            self.pixel_edit.addDocument(contents)
        # self.pixel_edit.layers.clear()
        # self.pixel_edit.layersUpdated()

    def load(self):
        filename = QFileDialog.getOpenFileName(self, 'Load image...', QStandardPaths.writableLocation(QStandardPaths.HomeLocation), "Image Files (*.png *.jpg *.mpx)")

        if filename[0]:
            if filename[0].endswith(".mpx"):
                mpx = MPX(filename[0], [])
                images = mpx.load()
            else:
                images = [QImage(filename[0])]

            for layer in self.pixel_edit.layers:
                self.pixel_edit.layers.delete_layer(layer)
            for image in images:
                self.pixel_edit.layers.add_layer(Layer(image))

            self.pixel_edit.layerView.layers = self.pixel_edit.layers
            self.pixel_edit.update()

    def export(self):
        filename = QFileDialog.getSaveFileName(self, 'Save image...', QStandardPaths.writableLocation(QStandardPaths.HomeLocation), "Image Files (*.png *.jpg)")
        if filename[0]:
            print(filename[0])
            self.pixel_edit.layers.full_image.save(filename[0])

    def save(self):
        # QStandardPaths.HomeLocation
        filename = QFileDialog.getSaveFileName(self, 'Save image...', QStandardPaths.writableLocation(QStandardPaths.HomeLocation), "Moonxel Files (*.mpx)")

        if filename[0]:
            images = map(lambda layer: layer.image, self.pixel_edit.layers)
            mpx = MPX(filename[0], images)
            mpx.save()


# TODO make this a separate UI element
class Toolbox(QWidget):
    pass


class Toolbar(QWidget):
    def __init__(self, toolboxes):
        super().__init__()

        self._toolboxes = toolboxes

    def initUI(self):
        self._bar = QVBoxLayout()
        for toolbox in self._toolboxes:
            self._bar.addWidget(toolbox)

        self.setLayout(self._bar)


class CanvasHolder(QWidget):
    def __init__(self, undo_stack):
        super().__init__()
        self._undo_stack = undo_stack

        self.canvases = []

        self.initUI()

    def initUI(self):
        self.tabWidget = QTabWidget()

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.tabWidget)

        self.setLayout(mainLayout)

    def addCanvas(self, parameters: dict):
        # self.layers = Layers(WIDTH, HEIGHT)
        #
        # for i in range(3):
        #     self.layers.add_layer()
        #
        # self.layers[1].sublayers.add_layer()
        #
        # self.layers[1].sublayers[0].sublayers.add_layer()
        # TODO make parameters a class? Dataclass Python 3.7?
        # ALSO PALETTE CURRENTLY UNUSED...
        properties = parameters.get("properties", {})

        if parameters["type"] == "template":
            layers = Layers(properties["width"], properties["height"])
            layers.add_layer(properties.get("background_color"))
        elif parameters["type"] == "file":
            # TODO load image
            # Also load palette from image (as option?)
            layers = Layers(properties["width"], properties["height"])
            layers.add_layer(properties.get("background_color"))
        # SAME AS
        # if parameters.get("type", "new") == "new":
        else:
            layers = Layers(properties["width"], properties["height"])
            layers.add_layer(properties.get("background_color"))
        canvas = Canvas(layers, self._undo_stack)
        self.canvases.append(canvas)
        self.tabWidget.addTab(canvas, properties.get("name", "New image"))

    @property
    def has_canvas(self):
        return bool(self.canvases)

    @property
    def canvas(self):
        return self.tabWidget.currentWidget()

    @property
    def layers(self):
        return self.tabWidget.currentWidget().layers

class PixelEdit(QWidget):
    # TODO make parent possible everywhere!
    def __init__(self, parent=None):
        super().__init__(parent)

        self.layerView = None
        # self.canvas = None
        self.paletteManager = None
        self.preview = None

        self.initUI()

        self.show()

    def initUI(self):
        self._undo_stack = UndoStack(max_size=9999999 + 1)
        # TODO keep track of currently in an action in progress...
        # all to be done in the undo stack
        self.undo_action = QAction("Undo", self)
        self.undo_action.setShortcut('Ctrl+Z')
        self.undo_action.triggered.connect(self._undo_stack.undo)
        self.addAction(self.undo_action)

        self.redo_action = QAction("Redo", self)
        self.redo_action.setShortcut('Ctrl+Shift+Z')
        self.redo_action.triggered.connect(self._undo_stack.redo)
        self.addAction(self.redo_action)

        self.rightSideBar = QVBoxLayout()
        self.leftSideBar = QVBoxLayout()
        self.box = QHBoxLayout()

        emptyLayers = Layers()

        self.layerView = LayerBar(emptyLayers)

        # TODO when document gets activated this should all be changed
        # The undo stack is part of the document structure.
        # When selecting a document, the UI should update around it, and each document deserves its own undo stack.
        # Current goal is getting it to work with one file.
        self.canvasHolder = CanvasHolder(self._undo_stack)

        # self.canvas = Canvas(self.layers, self._undo_stack)
        # self.canvas.updated.connect(self.layersUpdated)
        # self.canvas.updatedLayers.connect(self.layersUpdated)

        # self.layerView.activateLayer.connect(self.canvas.activateLayer)


        self.preview = Preview(emptyLayers)

        self.leftSideBar.addWidget(self.layerView)
        self.leftSideBar.addWidget(self.preview)

        self.box.addLayout(self.leftSideBar)
        self.box.addWidget(self.canvasHolder, stretch=2)

        self.toolbarbox = QHBoxLayout()
        self.leftMouseToolbar = Tools(side="left")
        self.rightMouseToolbar = Tools(side="right")
        self.leftMouseToolbar.newToolSelected.connect(self.toolbarNewToolSelected)
        self.rightMouseToolbar.newToolSelected.connect(self.toolbarNewToolSelected)
        self.toolbarbox.addWidget(self.leftMouseToolbar)
        self.toolbarbox.addWidget(self.rightMouseToolbar)

        self.optionsbox = QVBoxLayout()
        self.optionsbox.addLayout(self.toolbarbox)

        self.paletteManager = PaletteManager()
        self.paletteManager.newColorSelected.connect(self.palleteNewColorSelected)

        self.setColors()
        self.optionsbox.addWidget(self.paletteManager, stretch=2)
        self.box.addLayout(self.optionsbox)
        self.rightSideBar.addLayout(self.box)

        self.setLayout(self.rightSideBar)

    @property
    def canvas(self):
        if self.canvasHolder.has_canvas:
            return self.canvasHolder.canvas
        else:
            return None

    @property
    def layers(self):
        return self.canvas.layers

    def addDocument(self, parameters):
        self.canvasHolder.addCanvas(parameters)
        # self.canvas.updated.connect(self.layersUpdated)
        # self.canvas.updatedLayers.connect(self.layersUpdated)

        self.layers.updated.connect(self.layersUpdated)
        self.layers.updated_single.connect(self.singleLayerUpdated)

        self.layerView.layers = self.layers
        self.preview.layers = self.layers

        # TODO Should also move to properties of the current document...
        self.canvas.setColors(self.colors)

    # def canvasUpdated(self):
    #     self.layerView.update()

    def singleLayerUpdated(self):
        if self.canvas is not None:
            # self.canvas.scene.update()
            self.canvas.drawing.update()
        if self.preview is not None:
            self.preview.update()

    def layersUpdated(self):
        if self.layerView is not None:
            self.layerView.updateLayers()
        if self.canvas is not None:
            # self.canvas.scene.update()
            self.canvas.drawing.update()
        if self.preview is not None:
            self.preview.update()

    # def canvasUpdatedLayers(self):
    #     print("Updated layers", len(self.layers))
    #     self.layerView.layers = self.layers
    #     self.canvas.update()
    #     # self.layerView.update()

    def toolbarNewToolSelected(self, info):
        self.canvas.setTool(info["side"], info["tool"])

    def palleteNewColorSelected(self, side):
        self.colors[side] = self.paletteManager.currentPalette.colors[self.paletteManager.currentPalette.selected[side]]
        self.canvas.setColors(self.colors)

    def setColors(self):
        self.colors = {
            "left": self.paletteManager.currentPalette.colors[self.paletteManager.currentPalette.selected["left"]],
            "right": self.paletteManager.currentPalette.colors[self.paletteManager.currentPalette.selected["right"]],
        }

        if self.canvas is not None:
            self.canvas.setColors(self.colors)

    # def keyPressEvent(self, e):
    #     print(e)
    #     print(e.key())
    #     print(Qt.Key_Undo, Qt.Key_Escape)
    #
    #     if e.key() == Qt.Key_Undo:
    #         self.close()


def basic():
    import sys
    from PySide2.QtWidgets import QApplication

    app = QApplication(sys.argv)
    canvas = Easel()
    sys.exit(app.exec_())


if __name__ == '__main__':
    basic()
