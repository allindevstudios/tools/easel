from PySide2.QtCore import Signal, QPoint
from PySide2.QtGui import QPainter, QMouseEvent
from PySide2.QtWidgets import QFrame

from easel.data_structures import Palette


class Swatches(QFrame):
    colorClicked = Signal(int, int)
    plusClicked = Signal()

    def __init__(self, colors=None, swatch_size=18):
        super().__init__()

        self.swatch_size = swatch_size

        self.paletteData = [] if colors is None else colors

        self.initUI()

        self.show()

    def setPaletteData(self, paletteData: Palette):
        self.paletteData = paletteData
        self.repaint()

    def initUI(self):
        pass

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.paintSwatches(event, painter)
        painter.end()

    def paintSwatches(self, event, painter: QPainter):
        amount_of_columns = int(self.geometry().width() / self.swatch_size)
        if amount_of_columns == 0:
            return
        amount_of_rows = int((len(self.paletteData ) + 1) / amount_of_columns) + 1
        positions = [(i * self.swatch_size, j * self.swatch_size) for j in range(amount_of_rows) for i in range(amount_of_columns)]
        for color, position in zip(self.paletteData, positions):
            painter.fillRect(*position, self.swatch_size, self.swatch_size, color.qcolor)

        plusPosition = positions[len(self.paletteData)]
        offset = int(self.swatch_size / 2)

        painter.drawLine(plusPosition[0] + 2, plusPosition[1] + offset,
                         plusPosition[0] + self.swatch_size - 2, plusPosition[1] + offset)
        painter.drawLine(plusPosition[0] + offset, int(plusPosition[1] + 2),
                         plusPosition[0] + offset, int(plusPosition[1] + self.swatch_size - 2))

    def getSwatchIndex(self, position: QPoint) -> int:
        amount_of_columns = int(self.geometry().width() / self.swatch_size)
        if amount_of_columns == 0:
            return None
        row_index = int(position.y() / self.swatch_size)
        column_index = int(position.x() / self.swatch_size)
        index = row_index * amount_of_columns + column_index

        return index if index <= len(self.paletteData) else None

    def mousePressEvent(self, event: QMouseEvent):
        index = self.getSwatchIndex(event.pos())
        if index is None:
            return
        if index == len(self.paletteData):
            self.plusClicked.emit()
        else:
            self.colorClicked.emit(index, event.button())
