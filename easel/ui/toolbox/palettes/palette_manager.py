from PySide2.QtCore import Signal, Qt
from PySide2.QtGui import QColor
from PySide2.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QFrame, QFileDialog, QColorDialog, QComboBox

from easel.data_structures import Palette
from easel.ui.toolbox.palettes.swatches import Swatches
from easel.utility.files import Resources

from easel.data_structures.color import Color
from easel.utility.color import string_from_qcolor


def color_from_qcolor(color: QColor):
    color_hex = string_from_qcolor(color)
    return Color(color_hex)


class PaletteManager(QWidget):
    newColorSelected = Signal(str)

    def __init__(self, palettes=None):
        super().__init__()

        # TODO: lazy load of palette from file instead of all at once? To consider.
        self.palettes = palettes if palettes is not None else Resources().palettes

        self.initUI()

        self.show()

    def initUI(self):
        self.box = QVBoxLayout()
        self.setLayout(self.box)

        self.paletteManagement = QHBoxLayout()

        self.initPalettes()

        self.paletteManagement.addWidget(self.paletteComboBox)

        # TODO: draw icons in app :D
        self.resetPaletteButton = QPushButton("RESET")
        self.resetPaletteButton.clicked.connect(self.resetPaletteClicked)
        self.paletteManagement.addWidget(self.resetPaletteButton)

        self.savePaletteButton = QPushButton("SAVE")
        self.savePaletteButton.clicked.connect(self.savePaletteClicked)
        self.paletteManagement.addWidget(self.savePaletteButton)

        # TODO reload palettes to draw
        # self.reloadPalettesButton = QPushButton("RELOAD")
        # self.reloadPalettesButton.clicked.connect(self.resetPaletteClicked)
        # self.paletteManagement.addWidget(self.reloadPalettesButton)

        self.box.addLayout(self.paletteManagement)

        self.swatches = Swatches()
        self.swatches.colorClicked.connect(self.swatchColorClicked)
        self.swatches.plusClicked.connect(self.swatchPlusClicked)
        self.box.addWidget(self.swatches)


        self.colors = QHBoxLayout(self)

        self.leftClickColor = QFrame()
        self.leftClickColor.setFrameStyle(QFrame.StyledPanel | QFrame.Sunken)

        self.rightClickColor = QFrame()
        self.rightClickColor.setFrameStyle(QFrame.StyledPanel | QFrame.Sunken)

        self.updateSelectedColors()

        self.colors.addWidget(self.leftClickColor)
        self.colors.addWidget(self.rightClickColor)
        self.box.addLayout(self.colors)

        self.paletteComboBox.currentIndexChanged.connect(self.selectedPalette)
        self.paletteComboBox.setCurrentText("Nyx8")
        self.setGeometry(300, 300, 240, 300)

    def resetPaletteClicked(self):
        self.currentPalette.reset()
        self.update()
        self.updateSelectedColors()
        self.newColorSelected.emit("left")
        self.newColorSelected.emit("right")

    def savePaletteClicked(self):
        filename = QFileDialog.getSaveFileName(self, 'Save palette...', './data/palettes')

        if filename[0]:
            with open(filename[0], 'w') as f:
                f.write(self.currentPalette.file_representation)

    def swatchPlusClicked(self):
        self.currentPalette.colors.append(Color("#000000"))
        self.update()

    def updateSelectedColors(self):
        self.leftClickColor.setStyleSheet("QWidget { background-color: %s }"
                                          % self.currentPalette.colors[self.currentPalette.selected["left"]].qcolor.name())
        self.rightClickColor.setStyleSheet("QWidget { background-color: %s }"
                                           % self.currentPalette.colors[self.currentPalette.selected["right"]].qcolor.name())

    def updateSelected(self, side, colorIndex):
        self.currentPalette.selected[side] = colorIndex
        self.updateSelectedColors()
        self.newColorSelected.emit(side)

    def swatchColorClicked(self, colorIndex, button):
        if button == Qt.LeftButton:
            self.updateSelected("left", colorIndex)
        elif button == Qt.RightButton:
            self.updateSelected("right", colorIndex)
        elif button == Qt.MiddleButton:
            color = QColorDialog.getColor(options=QColorDialog.ShowAlphaChannel)
            if color.isValid():
                self.currentPalette.colors[colorIndex] = color_from_qcolor(color)
                if self.currentPalette.selected["left"] == colorIndex:
                    self.updateSelected("left", colorIndex)
                if self.currentPalette.selected["right"] == colorIndex:
                    self.updateSelected("right", colorIndex)

    def initPalettes(self):
        self.paletteComboBox = QComboBox()

        for palette in self.palettes:
            self.paletteComboBox.addItem(palette.name, palette)

    def selectedPalette(self, index):
        self.updateSelected("left", 0)
        self.updateSelected("right", len(self.currentPalette.colors) - 1)
        self.swatches.setPaletteData(self.currentPalette)

    @property
    def currentPalette(self) -> Palette:
        return self.paletteComboBox.currentData()

    # @property
    # def currentPalette(self):
    #     return self.paletteOption.itemData(self.paletteOption.currentIndex())
