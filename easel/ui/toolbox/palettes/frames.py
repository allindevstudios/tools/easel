from PySide2.QtCore import Signal, QPoint
from PySide2.QtGui import QPainter, QMouseEvent
from PySide2.QtWidgets import QFrame

swatch_size = 18


class Frames(QFrame):
    clicked = Signal(dict)

    def __init__(self, elements=None):
        super().__init__()

        self._elements = elements

        self.initUI()
        self.show()

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, new_elements):
        self._elements = new_elements()
        self.repaint()

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.paintElements(event, painter)
        painter.end()

    def paintElements(self, event, painter: QPainter):
        amount_of_columns = int(self.geometry().width() / swatch_size)
        if amount_of_columns == 0:
            return
        amount_of_rows = int((len(self.paletteData ) + 1) / amount_of_columns) + 1
        positions = [(i * swatch_size, j * swatch_size) for j in range(amount_of_rows) for i in range(amount_of_columns)]
        for color, position in zip(self.paletteData, positions):
            painter.fillRect(*position, swatch_size, swatch_size, color)

        plusPosition = positions[len(self.paletteData)]
        offset = int(swatch_size / 2)

        painter.drawLine(plusPosition[0] + 2, plusPosition[1] + offset,
                         plusPosition[0] + swatch_size - 2, plusPosition[1] + offset)
        painter.drawLine(plusPosition[0] + offset, int(plusPosition[1] + 2),
                         plusPosition[0] + offset, int(plusPosition[1] + swatch_size - 2))

    def getSwatchIndex(self, position: QPoint) -> int:
        amount_of_columns = int(self.geometry().width() / swatch_size)
        if amount_of_columns == 0:
            return None
        row_index = int(position.y() / swatch_size)
        column_index = int(position.x() / swatch_size)
        index = row_index * amount_of_columns + column_index

        return index if index <= len(self.paletteData) else None

    def mousePressEvent(self, event: QMouseEvent):
        index = self.getSwatchIndex(event.pos())
        if index is None:
            return
        if index == len(self.paletteData):
            self.plusClicked.emit()
        else:
            self.colorClicked.emit(index, event.button())
