# TODO find a better name

from PySide2.QtWidgets import QWidget, QPushButton, QHBoxLayout, QVBoxLayout, QComboBox, QFrame
from PySide2.QtGui import QPainter


from easel.utility.image import make_transparent_background


ZOOMS = [0.5, 1, 2]


def text_for_zoom(zoom):
    return '{}%'.format(int(zoom * 100))


class PreviewFrame(QFrame):
    def __init__(self, layers, scale=1):
        super().__init__()

        self._layers = layers
        self._background = make_transparent_background(self._layers.width, self._layers.height, grid_size=4)
        self._zoom = scale

        self.initUI()

        self.show()

    def initUI(self):
        self.setMinimumHeight(64)

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.paintImage(event, painter)
        painter.end()

    def updateZoom(self, newZoom):
        self._zoom = newZoom
        self.update()

    def paintImage(self, event, painter: QPainter):
        painter.drawImage(0, 0, self._background.scaledToWidth(self._background.width() * self._zoom))
        painter.drawImage(0, 0, self._layers.full_image.scaledToWidth(self._layers.width * self._zoom))


class Preview(QWidget):
    def __init__(self, layers):
        super().__init__()

        self._layers = layers

        self.initUI()

        self.show()

    @property
    def currentZoom(self):
        return self.options.currentData()

    def initUI(self):
        self.layoutBox = QVBoxLayout()
        self.options = QComboBox()
        for zoom in ZOOMS:
            self.options.addItem(text_for_zoom(zoom), zoom)

        self.previewFrame = PreviewFrame(self._layers)

        self.layoutBox.addWidget(self.previewFrame)
        self.layoutBox.addWidget(self.options)

        self.options.currentIndexChanged.connect(self.selectedZoomLevel)

        self.options.setCurrentIndex(1)

        self.setLayout(self.layoutBox)

    def selectedZoomLevel(self, index):
        self.previewFrame.updateZoom(self.currentZoom)
        
    @property
    def layers(self):
        return self._layers
    
    @layers.setter
    def layers(self, new_layers):
        self._layers = new_layers
        self.previewFrame._layers = self._layers
        self.update()


if __name__ == '__main__':
    import sys
    from PySide2.QtWidgets import QApplication

    app = QApplication(sys.argv)
    palette = WidgetTemplate()
    sys.exit(app.exec_())