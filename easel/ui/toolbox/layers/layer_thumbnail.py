from PySide2.QtCore import Signal, QPoint
from PySide2.QtGui import QPainter, QMouseEvent
from PySide2.QtWidgets import QFrame

from easel.data_structures import Layer
from easel.utility.image import make_transparent_background


class LayerThumbnail(QFrame):
    clicked = Signal()

    def __init__(self, layer: Layer):
        super().__init__()
        self._layer = layer
        self._transparent_background = make_transparent_background(32, 32)

        self.initUI()

    def initUI(self):
        # TODO find a way to add size?
        # TODO draw surrounding box when selected of course... all polish
        self.setFixedSize(32, 32)

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.paintImage(event, painter)
        painter.end()

    def paintImage(self, event, painter: QPainter):
        painter.drawImage(0, 0, self._transparent_background)
        painter.drawImage(0, 0, self._layer.image.scaledToWidth(32))

    def leftMousePressed(self, position: QPoint):
        # print("Clicked layer", self._layer.id)
        self.clicked.emit()

    def rightMousePressed(self, position: QPoint):
        pass
        # print("Right mouse clicked in layer", self._layer.id)

    def mousePressEvent(self, event: QMouseEvent):
        if event.button() == 1:
            self.leftMousePressed(event.pos())
        elif event.button() == 2:
            self.rightMousePressed(event.pos())