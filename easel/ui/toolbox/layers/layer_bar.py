from PySide2.QtCore import Signal
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QScrollArea

from easel.data_structures import Layers, Layer
from easel.ui.toolbox.layers.layer_element import LayerElement


class LayerSettings(QWidget):
    pass


class LayersActions(QWidget):
    addLayer = Signal()

    def __init__(self):
        super().__init__()

        self.initUI()

        self.show()

    def initUI(self):
        self.layout = QHBoxLayout()

        self.addLayerButton = QPushButton("+")
        self.addLayerButton.clicked.connect(self.addLayer.emit)
        self.layout.addWidget(self.addLayerButton)

        self.setLayout(self.layout)


class LayersLayout(QWidget):
    layerDeleted = Signal(Layer)
    layerActivated = Signal(Layer)

    def __init__(self, layers: Layers):
        super().__init__()
        self._layers = layers

        self.initUI()

        self.show()

    def initUI(self):
        self.layout = QVBoxLayout()
        self.paddingWidget = QWidget()

        self.layout.addWidget(self.paddingWidget, stretch=10000)

        self.layerViews = []

        for layer in self._layers:
            layerView = LayerElement(layer)
            layerView.layerDeleted.connect(self.layerDeleted.emit)
            layerView.layerActivated.connect(self.layerActivated.emit)
            self.layerViews.append(layerView)

        for layerView in self.layerViews[-1::-1]:
            self.layout.addWidget(layerView, alignment=Qt.AlignBottom)

        self.setLayout(self.layout)

    def updateLayers(self):
        self.layout.removeWidget(self.paddingWidget)

        for layerView in self.layerViews:
            self.layout.removeWidget(layerView)
            layerView.deleteLater()

        self.layerViews = []

        for layer in self._layers:
            layerView = LayerElement(layer)
            layerView.layerDeleted.connect(self.layerDeleted.emit)
            layerView.layerActivated.connect(self.layerActivated.emit)
            self.layerViews.append(layerView)

        self.layout.addWidget(self.paddingWidget, stretch=10000)

        for layerView in self.layerViews[-1::-1]:
            self.layout.addWidget(layerView, alignment=Qt.AlignBottom)

class LayerBar(QWidget):
    def __init__(self, layers: Layers):
        super().__init__()
        self._layers = layers

        self.initUI()

        self.show()

    @property
    def layers(self):
        return self._layers
    
    @layers.setter
    def layers(self, new_layers):
        self._layers = new_layers
        self.layersLayout._layers = self._layers
        self.updateLayers()

    def updateLayers(self):
        self.layersLayout.updateLayers()

    def layerDeleted(self, layer: Layer):
        self._layers.delete_layer(layer)

    def layerActivated(self, layer: Layer):
        self._layers.activate_layer(layer)

    def initUI(self):
        self.layout = QVBoxLayout()

        self.scrollArea = QScrollArea()

        self.layersLayout = LayersLayout(self._layers)
        self.layersLayout.layerActivated.connect(self.layerActivated)
        self.layersLayout.layerDeleted.connect(self.layerDeleted)


        self.scrollArea.setWidget(self.layersLayout)
        self.scrollArea.setWidgetResizable(True)

        self.layout.addWidget(self.scrollArea)

        self.layersActions = LayersActions()
        self.layersActions.addLayer.connect(self.addLayer)
        self.layout.addWidget(self.layersActions)

        self.setMinimumWidth(200)
        self.setLayout(self.layout)

    def addLayer(self):
        self._layers.add_layer()
    #
    # def paintEvent(self, event):
    #     painter = QPainter()
    #     painter.begin(self)
    #     self.paintImage(event, painter)
    #     painter.end()

    # def leftMousePressed(self, position: QPoint):
    #     print("Left mouse clicked in layer bar")
    #
    # def rightMousePressed(self, position: QPoint):
    #     print("Right mouse clicked in layer bar")
    #
    # def mousePressEvent(self, event: QMouseEvent):
    #     if event.button() == 1:
    #         self.leftMousePressed(event.pos())
    #     elif event.button() == 2:
    #         self.rightMousePressed(event.pos())