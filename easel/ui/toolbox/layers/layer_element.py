from PySide2.QtCore import Signal, Qt
from PySide2.QtGui import QPainter, QIcon
from PySide2.QtWidgets import QWidget, QHBoxLayout, QPushButton, QVBoxLayout

from easel.data_structures.layer import Layer
from easel.ui.toolbox.layers.layer_thumbnail import LayerThumbnail
from easel.utility.files import Resources


class LayerElement(QWidget):
    # TODO not sure if this works...
    layerDeleted = Signal(Layer)
    # TODO layer shouldn't decide what these buttons do per se... Naming them activated implies their activation...
    layerActivated = Signal(Layer)

    def __init__(self, layer: Layer):
        super().__init__()
        self._layer = layer

        self._layer.updated.connect(self.update)

        self.collapsed = False

        self.subLayerElements = []

        self.initUI()

    def minusButtonPressed(self):
        self.layerDeleted.emit(self._layer)

    def toggleVisibleButtonPressed(self):
        self._layer.toggle_visibility()
        if self._layer.visible:
            self.toggleVisibleButton.setIcon(QIcon(Resources().layer_icon_path("visible")))
        else:
            self.toggleVisibleButton.setIcon(QIcon(Resources().layer_icon_path("invisible")))
        self.toggleVisibleButton.setFlat(True)

    def initUI(self):
        self.layout = QVBoxLayout()
        self.mainLayout = QHBoxLayout()
        self.subLayersLayout = QVBoxLayout()

        if self._layer._sublayers:
            self.collapseButton = QPushButton(icon=QIcon(Resources().layer_icon_path("collapsed")) if self.collapsed else QIcon(Resources().layer_icon_path("folded")))
            self.collapseButton.setFlat(True)
            self.collapseButton.clicked.connect(self.toggleCollapse)
            self.mainLayout.addWidget(self.collapseButton)

        self.thumbnail = LayerThumbnail(self._layer)
        self.thumbnail.clicked.connect(self.thumbnailClicked)
        self.mainLayout.addWidget(self.thumbnail)

        # self.nameLabel = QLabel("{}".format(self._layer.name))
        # self.layout.addWidget(self.nameLabel)

        self.minusButton = QPushButton(icon=QIcon(Resources().layer_icon_path("trash")))
        self.minusButton.clicked.connect(self.minusButtonPressed)
        self.mainLayout.addWidget(self.minusButton)
        self.toggleVisibleButton = QPushButton(icon=QIcon(Resources().layer_icon_path("visible")) if self._layer.visible else QIcon(Resources().layer_icon_path("invisible")))
        self.toggleVisibleButton.clicked.connect(self.toggleVisibleButtonPressed)
        self.mainLayout.addWidget(self.toggleVisibleButton)

        self.layout.addLayout(self.mainLayout)

        self.layout.addLayout(self.subLayersLayout)

        self.setLayout(self.layout)

    def toggleCollapse(self):
        self.collapsed = not self.collapsed
        self.initSubLayers()

    def initSubLayers(self):
        if self._layer._sublayers:
            for layerLayout in self.subLayerElements:
                self.layout.removeItem(layerLayout)
                layerLayout.deleteLater()

            self.subLayerElements = []

            if self.collapsed:
                for layer in self._layer._sublayers:
                    layerLayout = QHBoxLayout()
                    paddingWidget = QWidget()
                    paddingWidget.setMinimumWidth(20)
                    layerLayout.addWidget(paddingWidget)
                    layerView = LayerElement(layer)
                    layerView.layerDeleted.connect(self.layerDeleted.emit)
                    layerView.layerActivated.connect(self.layerActivated.emit)
                    layerLayout.addWidget(layerView)
                    self.subLayerElements.append(layerLayout)

                for layerLayout in self.subLayerElements[-1::-1]:
                    self.subLayersLayout.addLayout(layerLayout)

                self.collapseButton.setIcon(QIcon(Resources().layer_icon_path("collapsed")))
            else:
                self.collapseButton.setIcon(QIcon(Resources().layer_icon_path("folded")))


    def thumbnailClicked(self):
        self.layerActivated.emit(self._layer)

    # TODO lots of updates here? Why so many paint Events here?... Might be nice to try out
    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        if self._layer.active:
            painter.fillRect(0, 0, self.width(), self.height(), Qt.gray)
        # # self.paintImage(event, painter)
        painter.end()