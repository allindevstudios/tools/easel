from PySide2.QtWidgets import QWidget, QPushButton, QHBoxLayout, QLabel
from PySide2.QtCore import Signal
from PySide2.QtGui import QMouseEvent, QIcon

from easel.data_structures.tool_data import ToolData
from easel.utility.files import Resources


class ToolButton(QWidget):
    toolClicked = Signal(dict, ToolData)

    def __init__(self, tool: ToolData):
        super().__init__()

        self.initUI()

        self.show()

    def initUI(self):
        pass

    def mousePressEvent(self, event: QMouseEvent):
        if event.button() == 1:
            self.leftMousePressed(event.pos())
        elif event.button() == 2:
            self.rightMousePressed(event.pos())


class Tools(QWidget):

    newToolSelected = Signal(dict)

    def __init__(self, side="left"):
        super().__init__()

        self.side = side

        self.initUI()

        self.show()

    def initUI(self):
        self.toolBarBox = QHBoxLayout()

        self.sideLabel = QLabel("L:" if self.side == "left" else "R:")
        self.toolBarBox.addWidget(self.sideLabel)

        self.thinPenButton = QPushButton(icon=QIcon(Resources().tool_icon_path("thin")))
        self.thinPenButton.clicked.connect(self.thinPenClicked)
        self.toolBarBox.addWidget(self.thinPenButton)
        self.thickPenButton = QPushButton(icon=QIcon(Resources().tool_icon_path("thick")))
        self.thickPenButton.clicked.connect(self.thickPenClicked)
        self.toolBarBox.addWidget(self.thickPenButton)
        self.eraserButton = QPushButton(icon=QIcon(Resources().tool_icon_path("eraser")))
        self.eraserButton.clicked.connect(self.eraserClicked)
        self.toolBarBox.addWidget(self.eraserButton)
        self.bucketButton = QPushButton(icon=QIcon(Resources().tool_icon_path("bucket")))
        self.bucketButton.clicked.connect(self.bucketClicked)
        self.toolBarBox.addWidget(self.bucketButton)

        # TODO reload palettes to draw
        # self.reloadPalettesButton = QPushButton("RELOAD")
        # self.reloadPalettesButton.clicked.connect(self.resetPaletteClicked)
        # self.paletteManagement.addWidget(self.reloadPalettesButton)

        self.setLayout(self.toolBarBox)

    def thinPenClicked(self, e: QMouseEvent):
        self.newToolSelected.emit({
            "side": self.side,
            "tool": "thin-pen"
        })

    def thickPenClicked(self, e: QMouseEvent):
        self.newToolSelected.emit({
            "side": self.side,
            "tool": "thick-pen"
        })

    def eraserClicked(self, e: QMouseEvent):
        self.newToolSelected.emit({
            "side": self.side,
            "tool": "eraser"
        })

    def bucketClicked(self, e: QMouseEvent):
        self.newToolSelected.emit({
            "side": self.side,
            "tool": "bucket"
        })



if __name__ == '__main__':
    import sys
    from PySide2.QtWidgets import QApplication

    app = QApplication(sys.argv)
    palette = Tools()
    sys.exit(app.exec_())