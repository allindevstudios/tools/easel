from typing import Mapping

from PySide2.QtCore import QPointF, QRectF, QPoint, Qt
from PySide2.QtGui import QPainter, QBrush, QColor, QPen, QMouseEvent
from PySide2.QtWidgets import QGraphicsItem, QGraphicsSceneMouseEvent

from easel.undo_stack.undoable_action import LayerChange
from easel.utility.color import qrgba_from_qcolor
from easel.utility.image import make_transparent_background, fill_on_position


class DrawingItem(QGraphicsItem):

    def __init__(self, layers, undo_stack, parent=None):
        super().__init__()
        self.parent = parent

        self._layers = layers
        self._undo_stack = undo_stack

        self._background = make_transparent_background(self._layers.width, self._layers.height, grid_size=2)

        # self.initUI()
        self.previousPosition = None

        # TODO move to undo stack itself...
        self._current_action = None

        self._pressed = {
            "left": False,
            "right": False,
            "middle": False,
        }

        self._tools = {
            "left": "bucket",
            "right": "thick-pen"
        }

        # TODO turn to selected tool of course
        self._brushes = dict()
        self._pens = dict()
        self._colors = dict()


        self.x_offset = 0
        self.y_offset = 0

        # self.setAcceptHoverEvents(True)
        # self.setAcceptedMouseButtons(Qt.LeftButton | Qt.RightButton)

        self._background = make_transparent_background(self._layers.width, self._layers.height)
        # self.setFlag(QGraphicsItem.ItemIsMovable)

    def getSizeRect(self):
        return QRectF(0, 0, self._layers.width, self._layers.height)

    def getSizeRectScaled(self):
        return QRectF(0, 0, self._layers.width * self.scale(), self._layers.height * self.scale())
        # return QRectF(self.transformedOffset.x(), self.transformedOffset.y(), self._layers.width * self.scale(), self._layers.height * self.scale())

    def boundingRect(self):
        return self.getSizeRect()

    def paint(self, painter, option, widget=None):
        # scene = self.scene()
        # boundingRect = scene.itemsBoundingRect()
        # sceneRect = scene.sceneRect()
        # rectSize = self.getSizeRect()
        # scale = self.scale()
        # print("SCENE RECT", sceneRect, "BOUNDING RECT", boundingRect)
        self.drawLayers(painter)

    def drawLayers(self, painter: QPainter):
        painter.drawImage(0, 0, self._background)
        painter.drawImage(0, 0, self._layers.full_image)

    @property
    def scaledWidth(self):
        return self.scale() * self._layers.width

    @property
    def scaledHeight(self):
        return self.scale() * self._layers.width

    @property
    def layers(self):
        return self._layers

    def updatePens(self):
        # TODO: these are now squares, make circles?
        self._brushes = {
            "left": QBrush(self._colors["left"].qcolor),
            "right": QBrush(self._colors["right"].qcolor),
        }

        self._pens = {}
        self.setPen("left")
        self.setPen("right")

    def setColors(self, colors: Mapping[str, QColor]):
        self._colors = colors

        self.updatePens()

    def setPen(self, side):
        if self._tools[side] == "thin-pen":
            self._pens[side] = QPen(self._brushes[side], 1)
        elif self._tools[side] == "thick-pen":
            self._pens[side] = QPen(self._brushes[side], 3, cap=Qt.RoundCap)
        elif self._tools[side] == "eraser":
            # TODO: figure out how this works cause annoying...
            pass
            # self.pens[side] = QPen(QBrush(QColor(0, 0, 0, 0)), 3)

    def setTool(self, side, tool):
        self._tools[side] = tool
        self.setPen(side)

    # def update(self):
    #     self.render.update()

    # def paintEvent(self, event):
    #     painter = QPainter()
    #     painter.begin(self)
    #     self.paintImage(event, painter)
    #     painter.end()

    # TODO when changing an active layer, all the previous ones and all the other ones can be batched together to optimize update rate.
    def paintImage(self, event, painter: QPainter):
        painter.drawImage(0, 0, self._background.scaledToWidth(self._background.width() * self.scale()))
        painter.drawImage(0, 0, self._layers.full_image.scaledToWidth(self._layers.width * self.scale()))

    def getPixelPosition(self, event: QGraphicsSceneMouseEvent):
        position = (event.scenePos() - self.scenePos()) / self.scale()
        pixelPosition = QPoint(int(position.x()), int(position.y()))
        # pixelPosition = QPoint(int(position.x()), int(position.y()))
        if pixelPosition.x() >= self.image.width() or pixelPosition.x() < 0 or pixelPosition.y() >= self.image.height() or pixelPosition.y() < 0:
            return None
        return pixelPosition

    @property
    def image(self):
        return self._layers.current_layer.image

    def paintOnPosition(self, side: str, position: QPoint):
        painter = QPainter()
        painter.begin(self.image)
        painter.setPen(self._pens[side])
        if self.previousPosition is None:
            painter.drawPoint(position)
        else:
            painter.drawLine(self.previousPosition, position)
        painter.end()

    def eraseOnPosition(self, position: QPoint):
        self.image.setPixel(position, qrgba_from_qcolor(QColor(0, 0, 0, 0)))

    # TODO make a class called "Edit" or so which has an appropriate UNDO/REDO action and can hold the current editing
    # in its own state for when needed. So for line drawing keeps an image. Then can also keep an 'edit preview' in this
    # edit and does not need to be in the layer itself...
    # Also need to work with render optimalisation because will need to have a certain way of knowing what layer is
    # being edited right now.
    def mouseMoved(self, event: QGraphicsSceneMouseEvent, mouseButton):
        pixelPosition = self.getPixelPosition(event)
        if pixelPosition is not None and self._layers.current_layer.is_drawable:
            if self._tools[mouseButton] == "eraser":
                self.eraseOnPosition(pixelPosition)
            elif self._tools[mouseButton] == "bucket":
                # TODO this is a good move in the right direction -> move edits to image helpers, not in canvas.
                # fill_on_position(self.image, self._colors[mouseButton], pixelPosition)
                fill_on_position(self.image, self._colors[mouseButton], pixelPosition)
            else:
                self.paintOnPosition(mouseButton, pixelPosition)
            # self.image.setPixel(self.getPixelPosition(position), self.colors["left"])

            # self.repaint()
            # to let the layers update
            self._layers.current_layer.updated_contents()
        self.previousPosition = pixelPosition

    def mouseMoveEvent(self, event: QGraphicsSceneMouseEvent):
        if self._pressed["left"]:
            self.mouseMoved(event, "left")
        if self._pressed["right"]:
            self.mouseMoved(event, "right")

    def mouseReleaseEvent(self, event: QMouseEvent):
        # TODO make helpers for the mouse functions
        if event.button() == 1:
            self.stopDrawing(event, "left")
        elif event.button() == 2:
            self.stopDrawing(event, "right")

    def stopDrawing(self, event: QMouseEvent, side: str):
        self._pressed[side] = False
        if self._current_action is not None:
            self._current_action.finish(self.image)
            self._current_action = None

    def startDrawing(self, event: 'QGraphicsSceneMouseEvent', side: str):
        self._pressed[side] = True
        self.previousPosition = None
        if self._current_action is not None and self._current_action.end_state is None:
            self._current_action.finish(self.image)
        action = LayerChange(self._layers.current_layer, self.image)
        self._undo_stack.add(action)
        self._current_action = action
        self.mouseMoved(event, side)

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent'):
        if event.button() == 1:
            self.startDrawing(event, "left")
        elif event.button() == 2:
            self.startDrawing(event, "right")
