from typing import Mapping

from PySide2.QtWidgets import QGraphicsView
from PySide2.QtCore import QRectF

from PySide2.QtGui import QColor

from easel.data_structures.layers import Layers
from easel.ui.canvas.canvas_item_render import DrawingItem
from easel.ui.canvas.canvas_scene import CanvasScene


class Canvas(QGraphicsView):
    SCALE = 1

    def __init__(self, layers: Layers, undo_stack):
        super().__init__()

        self._layers = layers
        self._undo_stack = undo_stack

        self.show()

        self.scene = CanvasScene(layers, undo_stack)

        self.initUI()

        self.setInteractive(True)

        self.show()

    def initUI(self):
        self.setScene(self.scene)

    @property
    def drawing(self):
        return self.scene.drawing

    @property
    def layers(self):
        return self._layers

    def updatePens(self):
        self.drawing.updatePens()

    def setColors(self, colors: Mapping[str, QColor]):
        self.drawing.setColors(colors)

    def setTool(self, side, tool):
        self.drawing.setTool(side, tool)



if __name__ == '__main__':
    import sys
    from PySide2.QtWidgets import QApplication

    app = QApplication(sys.argv)
    palette = Canvas()
    sys.exit(app.exec_())