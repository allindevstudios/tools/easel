from PySide2.QtWidgets import QGraphicsScene
from PySide2.QtWidgets import QGraphicsItem, QGraphicsSceneMouseEvent, QGraphicsSceneWheelEvent

from PySide2.QtCore import QPointF, QRectF, QPoint

from easel.data_structures.layers import Layers
from easel.ui.canvas.canvas_item_render import DrawingItem

class Scales:
    def __init__(self, start_scale=1):
        self._scales = [0.25, 0.5, 0.75, 1, 1.5, 2, 3, 4, 6, 8, 10, 12, 16, 20, 24]
        self._current_scale_index = self._scales.index(start_scale)
        self._current_scale = self._scales[self._current_scale_index]

    @property
    def current_scale(self):
        return self._scales[self._current_scale_index]

    def increase_scale(self):
        self._current_scale_index = min(len(self._scales) - 1, self._current_scale_index + 1)

    def decrease_scale(self):
        self._current_scale_index = max(0, self._current_scale_index - 1)


class CanvasScene(QGraphicsScene):
    def __init__(self, layers: Layers, undo_stack):
        super().__init__()

        self._pressed = {
            "left": False,
            "right": False,
            "middle": False,
        }

        self.moveStartPoint = None

        # TODO do away with this?
        self.startPosition = QPointF(0, 0)

        self.currentMoveOffset = QPointF(0, 0)

        self.transformedOffset = QPointF(0, 0)



        # TODO keep undo stack as a QObject somewhere and let the tools influence it?
        # TODO think hard about how to pass data to these lower objects?
        self._drawing = DrawingItem(layers, undo_stack)
        self._scales = Scales(8)
        self._drawing.setScale(self._scales.current_scale)
        self.addItem(self._drawing)

        self.updateSceneRect()

        self.centerDrawing()

    def centerDrawing(self):
        center = {
            "x": (self.width() - self._drawing.scaledWidth) / 2,
            "y": (self.height() - self._drawing.scaledHeight) / 2,
        }
        # self._drawing.setPos(center["x"], center["y"])
        self.transformedOffset = QPointF(center["x"], center["y"])
        self.setRenderPosition()

    @property
    def drawing(self) -> DrawingItem:
        return self._drawing

    def getPositionInView(self, point: QPointF) -> QPointF:
        limits = self.getDrawingMoveLimits()
        x = max(min(limits["right"], point.x()), limits["left"])
        y = max(min(limits["bottom"], point.y()), limits["top"])
        return QPointF(x, y)

    def setRenderPosition(self):
        combinedTransform = self.startPosition + self.transformedOffset + self.currentMoveOffset
        pointInView = self.getPositionInView(combinedTransform)
        self._drawing.setPos(pointInView)

    def getDrawingMoveLimits(self):
        factor = 1 / 3
        width = self._drawing.layers.width * self._drawing.scale()
        height = self._drawing.layers.height * self._drawing.scale()
        return {
            "left": width * (-factor),
            "right": width * (1 + factor),
            "top": height * (-factor),
            "bottom": height * (1 + factor),
        }

    def startMoving(self, event: QGraphicsSceneMouseEvent):
        self._pressed["middle"] = True
        self.moveStartPoint = event.scenePos()
        # self.moveCanvas(event)

    def stopMoving(self, event: 'QGraphicsSceneMouseEvent'):
        self.moveStartPoint = None
        self.transformedOffset = self.getPositionInView(self.transformedOffset + self.currentMoveOffset)
        self.currentMoveOffset = QPointF(0, 0)
        # self.updateSceneRect()
        self._pressed["middle"] = False

    def moveCanvas(self, event: QGraphicsSceneMouseEvent):
        self.currentMoveOffset = event.scenePos() - self.moveStartPoint
        # print("SP", event.scenePos(), "OFFSET", self.currentMoveOffset)
        # if self.previousPosition is not None:
        #     self.x_offset += (position.x() - self.previousPosition.x()) / 1 * self.scale()
        #     self.y_offset += (position.y() - self.previousPosition.y()) / 1 * self.scale()
        # self.previousPosition = position
        self.setRenderPosition()
        self.update()

    def updateSceneRect(self):
        # return
        scale = self._drawing.scale()
        scene_rect_x = self._drawing.layers.width * scale * 2
        scene_rect_y = self._drawing.layers.height * scale * 2
        self.setSceneRect(QRectF(0, 0, scene_rect_x, scene_rect_y))

        # startRect = self.itemsBoundingRect()
        # rect = QRectF(0, 0, startRect.x() + startRect.width(), startRect.y() + startRect.height())
        # TODO continue here

        # print(rect)
        # startRect.setX(0)
        # startRect.setY(0)
        # self.scene().setSceneRect(startRect)

    def sendDrawingEvent(self, event):
        self.sendEvent(self._drawing, event)

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent'):
        # render.mousePressEvent(event)
        if event.button() == 1:
            self.sendDrawingEvent(event)
            # self.startDrawing(event, "left")
        elif event.button() == 2:
            self.sendDrawingEvent(event)
            # self.startDrawing(event, "right")
        elif event.button() == 4:
            self.startMoving(event)
            # event.ignore()
        event.accept()

    def mouseMoveEvent(self, event: 'QGraphicsSceneMouseEvent'):
        if self._pressed["middle"]:
            self.moveCanvas(event)
        else:
            self.sendDrawingEvent(event)
        event.accept()

    def mouseReleaseEvent(self, event: 'QGraphicsSceneMouseEvent'):
        event.accept()
        if event.button() == 1:
            self.sendDrawingEvent(event)
            # self.startDrawing(event, "left")
        elif event.button() == 2:
            self.sendDrawingEvent(event)
            # self.startDrawing(event, "right")
        elif event.button() == 4:
            self.stopMoving(event)
            # event.ignore()

    # TODO: might be worth moving render things to here?
    def wheelEvent(self, event: 'QGraphicsSceneWheelEvent'):
        event.accept()

        vertical_speed = event.delta()
        if vertical_speed > 0:
            self._scales.increase_scale()
        else:
            self._scales.decrease_scale()

        first_pixel_position = (event.scenePos() - self._drawing.pos()) / self._drawing.scale()

        self._drawing.setScale(self._scales.current_scale)

        second_pixel_position = (event.scenePos() - self._drawing.pos()) / self._drawing.scale()
        offset_difference = (second_pixel_position - first_pixel_position) * self._drawing.scale()


        self.setRenderPosition()
        self.transformedOffset = self.getPositionInView(self.transformedOffset) + offset_difference

        self.updateSceneRect()

        self.setRenderPosition()
