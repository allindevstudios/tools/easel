from easel.tools.tool import Tool
from easel.utility.image import fill_on_position

from easel.data_structures.layers import Layers

class BucketTool(Tool):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def preview(self):
        raise NotImplemented

    def action(self, layers: Layers, position):
        fill_on_position(layers.current_layer, self._tool_data["color"], position)
