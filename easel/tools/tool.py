from easel.data_structures.tool_data import ToolData, EditToolData, SelectionToolData, TransformToolData


# TODO change init to allow subclass of tool data
class Tool:
    def __init__(self, tool_data: ToolData):
        self._tool_data = tool_data

    @property
    def name(self):
        return self._tool_data.name


class EditTool(Tool):
    def __init__(self, tool_data: EditToolData):
        super().__init__(tool_data)


class SelectionTool(Tool):
    def __init__(self, tool_data: SelectionToolData):
        super().__init__(tool_data)


class TransformTool(Tool):
    def __init__(self, tool_data: TransformToolData):
        super().__init__(tool_data)
