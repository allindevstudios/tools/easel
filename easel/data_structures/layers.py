from easel.data_structures.updated_data import UpdatedDataCollection
from easel.data_structures.image import Image
from easel.data_structures.layer import Layer

from easel.utility.image import make_new_image_32

from PySide2.QtGui import QPainter


# TODO make some utility to load layers from images. Decouple stuff more. Now a layer can't be loaded from an image either...
class Layers(UpdatedDataCollection):
    def __init__(self, width=32, height=32):
        super().__init__()
        self._width = width
        self._height = height

        self._full_image = None

        self._currently_active_layer = None

        self._layers = []
        # TODO not sure if this is the best way... but it allows for quick access ID wise
        self._layers_hash = dict()

    def add_layer(self, new_layer: Layer = None, background_color=None):
        # TODO  maybe get ", self._fill_color" from another data layer
        image = make_new_image_32(self._width, self._height, fill_color=background_color)
        new_layer = Layer(image) if new_layer is None else new_layer

        self._add_updated_data(new_layer)

        self._layers.append(new_layer)
        self._layers_hash[new_layer.id] = new_layer

        self.activate_layer(new_layer)

        self._on_updated()

    # TODO move this to the mouse buttons themselves.
    # Or keep it here as well? Data needs to be doubled but centralised as well...
    def activate_layer(self, layer: Layer):
        # if the signals become a problem, might be cool to disconnect and connect (and put this in a decorator?)
        if self._currently_active_layer is not None:
            self._currently_active_layer.active = False

        self._currently_active_layer = layer
        self._currently_active_layer.active = True

    def delete_layer(self, layer: Layer):
        layer_index = self._layers.index(layer)
        del self._layers[layer_index]
        del self._layers_hash[layer.id]

        if layer.active and len(self._layers) > 1:
            self.activate_layer(self._layers[min(layer_index, len(self._layers) - 1)])

        self._remove_updated_data(layer)

        self._on_updated()

    def clear(self):
        for layer in self._layers:
            layer.clear()

    @property
    def current_layer(self):
        return self._currently_active_layer

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def layers(self):
        return self._layers

    @property
    def full_image(self):
        if self._full_image is None:
            self._make_full_image()
        return self._full_image

    # TODO: actually add these to one another and then only paint them (also check on visibility when added) AAAH SO MUCH FUN
    # TODO: batch the ones where change didn't happen (current layer) to reduce redrawing
    # Like batching them in groups of 5/10/15 and then only recalculating that group and redrawing batched?
    # Also benchmark to see if this is actually necessary...
    def _make_full_image(self):
        self._full_image = make_new_image_32(self._width, self._height)
        painter = QPainter(self._full_image)
        for layer in self._layers:
            if not layer.visible:
                continue
            # painter.drawImage(0, 0, layer.scaledToWidth(layer.width()))
            painter.drawImage(0, 0, layer.image)

    def _on_updated(self):
        self._make_full_image()
        super()._on_updated()

    def _on_updated_single(self):
        self._make_full_image()
        super()._on_updated_single()
        
    def __iter__(self):
        return iter(self._layers)

    def __len__(self):
        return len(self._layers)

    def __bool__(self):
        return bool(self._layers)

    def __getitem__(self, item):
        return self._layers[item]
