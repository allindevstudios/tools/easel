import uuid

from easel.data_structures.updated_data import UpdatedData
from easel.data_structures.image import Image

from easel.utility.image import get_colors_from_image, clear_image

import easel.data_structures.layers as layersData



class Layer(UpdatedData):
    def __init__(self, image: Image, name: str = None):
        super().__init__()
        self._id = uuid.uuid4()
        self._name = "No name" if name is None else name
        self._image = image
        self._edit_preview = None
        self._alpha = 1
        self._active = False
        self._visible = True

        self._sublayers = layersData.Layers(width=self._image.width(), height=self._image.height())
        self._sublayers.updated.connect(self.updated.emit)
        self._sublayers.updated_single.connect(self.updated.emit)

    def clear(self):
        clear_image(self._image)

    def toggle_visibility(self):
        self.visible = not self._visible

    def load(self, data: dict):
        pass

    def start_edit(self):
        self._edit_preview = make_new_image_32

    # TODO shouldn't happen like this, editable_layer_preview should be added or so...
    def updated_contents(self):
        self._on_updated()

    @property
    def save_data(self) -> dict:
        return {}

    @property
    def sublayers(self) -> "layersData.Layers":
        return self._sublayers

    @property
    def is_drawable(self) -> bool:
        return not self._sublayers

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def width(self):
        return self._image.width()

    @property
    def height(self):
        return self._image.height()

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, new_alpha):
        self._alpha = new_alpha
        self._on_updated()

    @property
    def image(self):
        if self._sublayers:
            return self._sublayers.full_image
        return self._image

    # May be useful when making a layer from an imported image or so...
    @image.setter
    def image(self, new_image):
        self._image = new_image
        self._on_updated()

    # TODO make a per button basis instead of per layer!
    # But know that it is pretty handy to know whether a layer is active in the layer itself?
    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, value):
        self._active = value
        self._on_updated()

    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, new_visible):
        self._visible = new_visible
        self._on_updated()

    @property
    def colors(self):
        return get_colors_from_image(self._image)

    def __hash__(self):
        return self._id
