from easel.data_structures.updated_data import UpdatedData
from easel.data_structures.color import Color
from easel.data_structures.tool_data import ToolData


# TODO:
# - in this class, the controls are managed for things that are not buttons:
# - so if a layer is clicked, this would manage it and update the selected data.
# - this way, it may be easy to add new elements and get the interaction solidly.
# now the question is, is to make this listen to signals or be passed as a parameter. Ideally, it'd connect.
class MouseButton:
    pass


class SelectionUpdate:
    def __init__(self, type_, button, ):
        self._type = type_
        self._button = button


# TODO maybe make a tool
class ButtonSelected(UpdatedData):
    def __init__(self):
        super().__init__()
        self._color = None
        self._tool = None
        self._layer = None

    @property
    def color(self):
        return self._color
    
    @color.setter
    def color(self, new_color: Color):
        self._color = new_color
        self._on_updated()

    @property
    def tool(self):
        return self._tool
    
    @tool.setter
    def tool(self, new_tool: ToolData):
        self._tool = new_tool
        self._on_updated()

    def update(self, data: SelectionUpdate):
        # TODO
        pass


class Selected(UpdatedData):
    def __init__(self):
        super().__init__()
        self._left = ButtonSelected()
        self._right = ButtonSelected()
        self._left.updated.connect(self._on_updated)
        self._right.updated.connect(self._on_updated)

    @property
    def left(self):
        return self._left

    @property
    def right(self):
        return self._right

    # TODO Load previously selected tools on shutdown from file.
    def load(self, data: dict):
        pass

    @property
    def save_data(self):
        return {
            "left": {
                "color": self._left.color

            },
            "right": {}
        }

    def update(self, data: SelectionUpdate):
        pass