from easel.data_structures.palette import Palette
from easel.data_structures.layers import Layers
from easel.data_structures.selected import Selected


# TODO: not sure if I'll do it this way
class EaselFileData:
    def __init__(self):
        self._palette = Palette()
        self._layers = Layers()
        self._selected = Selected()

    def load(self):
        pass
