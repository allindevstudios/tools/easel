from PySide2.QtCore import Signal, QObject


class UpdatedData(QObject):
    updated = Signal()

    def __init__(self):
        super().__init__()

    def _on_updated(self):
        self.updated.emit()


class UpdatedDataCollection(UpdatedData):
    updated_single = Signal()

    def __init__(self):
        super().__init__()

    def _add_updated_data(self, updated_data):
        updated_data.updated.connect(self._on_updated_single)

    def _remove_updated_data(self, updated_data):
        updated_data.updated.disconnect(self._on_updated_single)

    def _on_updated_single(self):
        self.updated_single.emit()
