import os

from easel.data_structures.updated_data import UpdatedData
from easel.data_structures.color import Color

from easel.utility.color import string_from_qcolor


class Palette(UpdatedData):
    def __init__(self, filename: str):
        super().__init__()
        self.filename = filename
        self._colors = []

        # TODO: change to colors, and also make property in Qt instead of here? Needs to be discussed.
        self.selected = {
            "left": 0,
            "right": 0,
        }
        self._name = os.path.basename(filename).capitalize()
        self.load_from_file()

        # TODO was here
        # print(list(map(lambda x: hex(x), self._qrgba_colors)))

    def load_from_file(self):
        with open(self.filename, 'r') as f:
            # TODO change this? How often are these calls performed?
            self._colors = [Color(line.strip()) for line in f.readlines()]
        self._on_updated()

    def reset(self):
        """Resets the palette to its original data"""
        self.selected = {
            "left": 0,
            "right": 0,
        }

        self._name = os.path.basename(self.filename).capitalize()
        self.load_from_file()

    @property
    def name(self):
        return self._name

    @property
    def colors(self):
        return self._colors

    @property
    def file_representation(self):
        s = ''
        for color in self:
            s += string_from_qcolor(color.qcolor)[1:] + "\n"
        return s

    def __len__(self):
        return len(self.colors)

    def __iter__(self):
        return iter(self.colors)
