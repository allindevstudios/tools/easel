from PySide2.QtGui import QColor
from easel.utility.color import qcolor_from_string, qrgba_from_qcolor

# TODO make like Image(QImage) -> Color(QColor)
# PROBLEM: messes up with Palette, need obvious setter


class Color:
    def __init__(self, color: str):
        self._color = color
        self._qcolor = qcolor_from_string(color)
        self._qrgba = qrgba_from_qcolor(self._qcolor)

    @property
    def color(self):
        return self._color
    
    @color.setter
    def color(self, color):
        self._color = color
        self._qcolor = QColor(color)
        self._qrgba = self._qcolor.rgba()

    @property
    def qcolor(self):
        return self._qcolor

    @property
    def qrgba_color(self):
        return self._qrgba

    def __eq__(self, other):
        return self.qrgba_color == other.qrga_color
