import queue
import collections
from time import time

from PySide2.QtGui import QImage, QColor, qRgba, QPainter, QPen, QBrush
from PySide2.QtCore import QPoint
from easel.data_structures.color import Color
from easel.data_structures.image import Image
# TODO unit tests of course


def clear_image(image: QImage):
    image.fill(QColor(0, 0, 0, 0))


def make_new_image_32(width: int, height: int, fill_color: QColor = None) -> Image:
    fill_color = QColor(0, 0, 0, 0) if fill_color is None else fill_color

    image = Image(width, height, QImage.Format_ARGB32)
    image.fill(fill_color)
    return image


def copy_image(image: Image) -> Image:
    return Image(image)


def make_new_image_8(width: int, height: int, fill_color: QColor = None) -> Image:
    fill_color = QColor(0, 0, 0, 0) if fill_color is None else fill_color

    image = Image(width, height, QImage.Format_Indexed8)
    image.fill(fill_color)
    return image


def make_transparent_background(width: int, height: int, grid_size:int = 1) -> Image:
    fill_color = QColor(220, 220, 220)
    darker_color = QColor(150, 150, 150)
    image = make_new_image_32(width, height, fill_color)

    dark_brush = QBrush(darker_color)
    dark_pen = QPen(dark_brush, grid_size)

    painter = QPainter()
    painter.begin(image)
    painter.setPen(dark_pen)
    for x in range(grid_size // 2, width, grid_size):
        for y in range(grid_size // 2 + ((x // grid_size) % 2) * grid_size, height, 2 * grid_size):
            painter.drawPoint(x, y)

    return image


def make_new_image(width: int, height: int, fill_color: QColor = None) -> Image:
    fill_color = QColor(0, 0, 0, 0) if fill_color is None else fill_color

    image = Image(width, height, QImage.Format_Indexed8)
    image.fill(fill_color)
    return image


def image_from_source(src) -> Image:
    return Image(src)


def coordinates_in_image(image: QImage) -> iter:
    for x in range(image.width()):
        for y in range(image.height()):
            yield x, y


def colors_in_image(image: QImage) -> iter:
    for x, y in coordinates_in_image(image):
        yield hex(image.pixel(x, y))


def get_colors_from_image(image: QImage) -> list:
    colors = set()
    for color in colors_in_image(image):
        colors.add(color)
    return list(colors)


def get_color_indexes_from_image(image: QImage) -> dict:
    colors = get_colors_from_image(image)
    return {color: index for index, color in enumerate(colors)}


# TODO: try working with color tables
def change_color_in_image(image: QImage, original_color: QColor, new_color: QColor):
    for x, y in coordinates_in_image(image):
        color = QColor(image.pixel(x, y))
        if color == original_color:
            image.setPixelColor(x, y, new_color)


def get_color_in_image(image: Image, position: QPoint):
    # print(image.pixel(position))
    return QColor(image.pixel(position))


# TODO https://en.wikipedia.org/wiki/Predication_(computer_architecture)
def fill_on_position_predication(image: Image, new_color: Color, position: QPoint):
    from time import time
    start = time()
    raise NotImplementedError("TODO!")


def fill_on_position(image: Image, new_color: Color, position: QPoint):
    # start = time()

    original_color = image.pixel(position)
    new_color_qcolor = new_color.qcolor

    if original_color == new_color.qrgba_color:
        return False

    visited = set()
    to_visit = set()

    image_width = image.width() - 1
    image_height = image.height() - 1

    to_visit.add(100000 * position.x() + position.y())
    while to_visit:
        location = to_visit.pop()
        y = location % 100000
        x = location // 100000
        if image.pixel(x, y) == original_color:
            image.setPixelColor(x, y, new_color_qcolor)
            location_hash = 100000 * (x - 1) + y
            if x > 0 and location_hash not in visited:
                to_visit.add(location_hash)
            location_hash += 200000
            if x < image_width and location_hash not in visited:
                to_visit.add(location_hash)
            location_hash -= 100001
            if y > 0 and location_hash not in visited:
                to_visit.add(location_hash)
            location_hash += 2
            if y < image_height and location_hash not in visited:
                to_visit.add(location_hash)
        visited.add(location)
    # print("Completed in ", time() - start)


if __name__ == '__main__':
    pass
