from PySide2.QtGui import QColor


def qrgba_from_qcolor(color: QColor) -> int:
    return color.rgba()


def qcolor_from_string(color_hex: str) -> QColor:
    # add # character if not present
    if not color_hex.startswith("#"):
        color_hex = "#" + color_hex
    # add alpha if not present
    if not len(color_hex) >= 8:
        color_hex = color_hex[0] + "ff" + color_hex[1:]

    return QColor(color_hex)


def string_from_qcolor(color: QColor) -> str:
    return "#" + hex(color.rgba())[2:]


if __name__ == '__main__':
    pass
