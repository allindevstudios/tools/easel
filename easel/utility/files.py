import os
import pkg_resources

from easel.data_structures.palette import Palette


# TODO just an idea to do it like this... Will probably change in the future or be a component of the main
# widget which is used to instruct the other widgets (like load palettes from this)
# Ideally, the less autonomous a widget like Palettes is in its loading, the better?
class Resources:
    def __init__(self, user_data_path=None):
        self._default_data_path = pkg_resources.resource_filename("easel", "data/")
        self._user_data_path = user_data_path
    
    @property
    def palettes(self):
        return [Palette(os.path.join(self.palettes_path, palette)) for palette in
                self.default_palettes]

    @property
    def palettes_path(self):
        return os.path.join(self._default_data_path, 'palettes')

    @property
    def default_palettes(self):
        return os.listdir(self.palettes_path)

    @property
    def tool_icons_path(self):
        return os.path.join(self.icons_path, 'tools')

    def tool_icon_path(self, tool_name: str):
        return os.path.join(self.tool_icons_path, tool_name)

    @property
    def layer_icons_path(self):
        return os.path.join(self.icons_path, 'layers')

    def layer_icon_path(self, icon_name: str):
        return os.path.join(self.layer_icons_path, icon_name)

    @property
    def icons_path(self):
        return os.path.join(self._default_data_path, 'icons')

    @property
    def main_icon(self):
        return os.path.join(self.icons_path, 'icon')