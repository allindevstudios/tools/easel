from easel.utility.image import copy_image


class UndoableAction:
    def __init__(self, applicable_object, start_state=None):
        self._applicable_object = applicable_object
        self._start_state = start_state
        self._end_state = None

        self._post_process_start_state()

    def _post_process_start_state(self):
        pass

    def _post_process_end_state(self):
        pass

    def finish(self, end_state):
        self._end_state = end_state
        self._post_process_end_state()

    def undo(self):
        if self._start_state is not None:
            self.apply(self._start_state)

    def redo(self):
        if self._end_state is not None:
            self.apply(self._end_state)

    def apply(self, new_state):
        raise NotImplementedError

    @property
    def start_state(self):
        return self._start_state

    @property
    def end_state(self):
        return self._end_state


class LayerChange(UndoableAction):

    def _post_process_start_state(self):
        self._start_state = copy_image(self._start_state)

    def _post_process_end_state(self):
        self._end_state = copy_image(self._end_state)

    def apply(self, new_state):
        self._applicable_object.image = copy_image(new_state)


# TODO actions like these can be done via another way. Maybe pass the undo and redo?
# TODO index is not kept in here... must rethink
# TODO maybe using setter which disconnects and connects all layers...
# TODO or add a index to a layer and that way sort it on add?
class DeleteLayer(UndoableAction):
    def undo(self):
        self._applicable_object.add_layer(self._start_state)

    def redo(self):
        self._applicable_object.delete_layer(self._start_state)
