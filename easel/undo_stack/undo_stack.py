class UndoStack:
    def __init__(self, max_size=128):
        self._max_size = max_size
        self._stack = []
        self._pointer = len(self._stack) - 1

    def add(self, action):
        if self._stack:
            self._stack = self._stack[:self._pointer + 1] + [action]
        else:
            self._stack.append(action)
        self._pointer += 1

    def undo(self):
        if self._pointer >= 0:
            self.current_undo_action.undo()
            self._pointer -= 1
            return True
        return False

    def redo(self):
        if self._pointer < len(self._stack) - 1:
            self.current_redo_action.redo()
            self._pointer += 1
            return True
        return False

    @property
    def current_undo_action(self):
        return self._stack[self._pointer]

    @property
    def current_redo_action(self):
        return self._stack[self._pointer + 1]

    def reset_to_start(self):
        while self.undo():
            pass

    # TODO this pair will be pretty hard
    def save(self) -> dict:
        pass

    def load(self, data: dict):
        pass