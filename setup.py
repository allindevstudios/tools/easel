from setuptools import setup

setup(
    name='easel',
    version='0.1.0',
    packages=['easel', 'easel.ui', 'easel.ui.canvas', 'easel.ui.toolbox', 'easel.mpx', 'easel.utility',
              'easel.data_structures', 'easel.undo_stack'],
    url='',
    license='',
    include_package_data=True,
    author='Laurent Van Acker',
    author_email='laurentvanacker@gmail.com',
    description='Pixelart editor made in PyQt5',
    entry_points={
        'console_scripts': [
            'easel = easel.main:basic'
        ],
        'gui_scripts': [
            'easel-basic = easel.main:basic'
        ]
    },
)
