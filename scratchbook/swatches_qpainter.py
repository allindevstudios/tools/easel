#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we draw text in Russian Cylliric.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QColor, QFont
from PyQt5.QtCore import Qt

from random import random


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.text = "Лев Николаевич Толстой\nАнна Каренина"

        self.setGeometry(300, 300, 1000, 1000)
        self.setWindowTitle('Drawing text')
        self.show()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        self.paintSwatches(event, qp)
        qp.end()

    def paintSwatches(self, event, qp: QPainter):
        size = 15
        colors = [QColor(int(256 * random()), int(256 * random()), int(256 * random())) for i in range(10000)]
        positions = [(i * size, j * size) for i in range(100) for j in range(100)]
        for color, position in zip(colors, positions):
            # qp.setPen(color)
            qp.fillRect(*position, size, size, color)
            # qp.drawRect(*position, size, size)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())