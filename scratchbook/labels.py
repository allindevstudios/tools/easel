#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we draw text in Russian Cylliric.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton, QFrame
from PyQt5.QtGui import QPainter, QColor, QFont, QPen, QMouseEvent
from PyQt5.QtCore import Qt

from random import random


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.text = "Лев Николаевич Толстой\nАнна Каренина"

        self.setGeometry(300, 300, 1000, 1000)

        self.button = QPushButton("HELLO", self)

        self.frame = QFrame(self)
        self.frame.move(30, 30)
        self.frame.setGeometry(0, 0, 500, 500)

        self.setWindowTitle('Drawing text')
        self.show()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        self.paintLines(event, qp)
        qp.end()

    def paintLines(self, event, qp: QPainter):
        print("Painting lines!")
        qp.setPen(QPen(Qt.white, 2, Qt.SolidLine))
        qp.drawLine(0, 0, 1000, 1000)

    def paintPoint(self, point):
        qp = QPainter()
        qp.begin(self)
        qp.setPen(QPen(Qt.white, 2, Qt.SolidLine))
        qp.drawPoint(100, 100)
        qp.end()

    def mousePressEvent(self, e: QMouseEvent):
        self.paintPoint([e.x, e.y])



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())