#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we create a skeleton
of a calculator using QGridLayout.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import (QWidget, QGridLayout,
                             QPushButton, QApplication, QFrame)
from PyQt5.QtGui import QColor

from random import random

class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def makeFrame(self):
        color = QColor(int(256 * random()), int(256 * random()), int(256 * random()), 0)
        frame = QFrame(self)
        frame.setStyleSheet("QWidget { background-color: %s }"
            % color.name())
        frame.setGeometry(0, 0, 20, 20)
        return frame

    def initUI(self):

        grid = QGridLayout()
        self.setLayout(grid)

        frames = [self.makeFrame() for i in range(10000)]
        positions = [(i, j) for i in range(100) for j in range(100)]

        for position, frame in zip(positions, frames):
            grid.addWidget(frame, *position)

        self.setGeometry(500, 500, 1000, 500)
        self.setWindowTitle('Swatches')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())