from scratchbook.colors_ui import Ui_Colors

from PyQt5.QtWidgets import QApplication, QMainWindow


class ApplicationTest(QMainWindow):
    def __init__(self):
        super().__init__()

        self.colorsDock = Ui_Colors()
        self.colorsDock.setupUi(self)

        self.show()

if __name__ == '__main__':
    import sys
    
    app = QApplication(sys.argv)
    mainWindow = ApplicationTest()
    mainWindow.show()
    sys.exit(app.exec_())
