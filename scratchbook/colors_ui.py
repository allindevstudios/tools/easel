# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'colors.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Colors(object):
    def setupUi(self, Colors):
        Colors.setObjectName("Colors")
        Colors.resize(400, 300)
        Colors.setFeatures(QtWidgets.QDockWidget.AllDockWidgetFeatures)
        Colors.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)

        self.retranslateUi(Colors)
        QtCore.QMetaObject.connectSlotsByName(Colors)

    def retranslateUi(self, Colors):
        _translate = QtCore.QCoreApplication.translate
        Colors.setWindowTitle(_translate("Colors", "&Colors"))

