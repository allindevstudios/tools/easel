# TODO find a better name

from PyQt5.QtWidgets import QWidget, QPushButton, QHBoxLayout, QVBoxLayout
from PyQt5.Qt import pyqtSignal


class WidgetTemplate(QWidget):

    signalDummy = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.initUI()

        self.show()

    def initUI(self):
        pass


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    palette = WidgetTemplate()
    sys.exit(app.exec_())