from easel.utility.color import qcolor_from_string, qrgba_from_qcolor, string_from_qcolor


def assert_correct_colors(color, red, green, blue, alpha=None):
    assert color.red() == red
    assert color.green() == green
    assert color.blue() == blue
    if alpha is not None:
        assert color.alpha() == alpha


def test_qcolor_from_string():
    qcolor = qcolor_from_string("123456")
    assert_correct_colors(qcolor, 0x12, 0x34, 0x56, 0xff)
    qcolor = qcolor_from_string("#123456")
    assert_correct_colors(qcolor, 0x12, 0x34, 0x56, 0xff)
    qcolor = qcolor_from_string("#ff123456")
    assert_correct_colors(qcolor, 0x12, 0x34, 0x56, 0xff)
    qcolor = qcolor_from_string("#01341256")
    assert_correct_colors(qcolor, 0x34, 0x12, 0x56, 0x01)


def test_string_from_qcolor():
    qcolor = qcolor_from_string("123456")
    assert string_from_qcolor(qcolor) == "#ff123456"
    qcolor = qcolor_from_string("#ff123456")
    assert string_from_qcolor(qcolor) == "#ff123456"
    qcolor = qcolor_from_string("#12123456")
    assert string_from_qcolor(qcolor) == "#12123456"

