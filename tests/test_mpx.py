import pytest

from PyQt5.QtGui import QImage

from easel.mpx.mpx import MPX
from easel.mpx.utility import make_colors_from_layer, make_contents_from_layer


@pytest.fixture
def layers():
    first_layer = QImage('icon.png')
    second_layer = QImage('other_icon.png')
    return [first_layer, second_layer]


@pytest.fixture
def layer_without_transparency():
    return [QImage('icon.png')]


@pytest.fixture
def layer_with_transparency():
    return [QImage('other_icon.png')]

@pytest.fixture
def stress_test():
    return [QImage('larger_file.png'), QImage('larger_file.png'), QImage('larger_file.png')]

@pytest.fixture
def realistic_file():
    return [QImage('realistic_file.png')]


def test_mpx_initialization(layers):
    mpx = MPX("test.png", layers)
    assert mpx.filename == "test.mpx"
    assert len(mpx.layers) == 2
    assert mpx.layers == layers


def test_correct_extension():
    mpx = MPX("beautiful.png", [])
    assert mpx.filename == "beautiful.mpx", "Wrong filename"

    mpx = MPX("/home/laurent/another_image.v2.jpeg", [])
    assert mpx.filename == "/home/laurent/another_image.v2.mpx", "Wrong filename"

    mpx.filename = "/home/laurent/another_image.v2.jpeg"
    assert mpx.filename == "/home/laurent/another_image.v2.mpx", "Wrong filename"


def test_correct_colors_made_no_transparency(layer_without_transparency):
    mpx = MPX("test.png", layer_without_transparency)
    assert len(mpx.colors) > 0
    assert len(mpx.colors) == len(make_colors_from_layer(layer_without_transparency[0]))


def test_correct_colors_made_transparency(layer_with_transparency):
    mpx = MPX("test.png", layer_with_transparency)
    assert len(mpx.colors) > 0
    assert len(mpx.colors) == len(make_colors_from_layer(layer_with_transparency[0]))


def test_correct_colors_two_layers(layers):
    mpx = MPX("test.png", layers)
    assert len(mpx.colors) > 0
    assert len(mpx.colors) <= len(make_colors_from_layer(layers[0])) + len(make_colors_from_layer(layers[1]))
    assert len(mpx.colors) >= len(make_colors_from_layer(layers[0]))
    assert len(mpx.colors) >= len(make_colors_from_layer(layers[1]))


def test_make_contents_per_layer(layers):
    contents = make_contents_from_layer(layers[0], make_colors_from_layer(layers[0]))
    assert len(contents) > 0
    contents = make_contents_from_layer(layers[1], make_colors_from_layer(layers[1]))
    assert len(contents) > 0

def test_make_contents(layer_with_transparency):
    mpx = MPX("test.png", layer_with_transparency)
    assert len(mpx.contents) > 0

def test_save_data(layers):
    mpx = MPX("test.png", layers)
    print(mpx.save_data)
    assert len(mpx.save_data.split('\n')) == 5

def test_save(layers):
    mpx = MPX("test.png", layers)
    mpx.save()

def test_stress_test(stress_test):
    mpx = MPX("stress.mpx", stress_test)
    mpx.save()

def test_realistic_file(realistic_file):
    mpx = MPX("realistic.mpx", realistic_file)
    mpx.save()

def test_load_file():
    mpx = MPX("test.mpx", [])
    images = mpx.load()
    assert len(images) > 0



